<?php
# =========== STYLE LOGIN =========== #
function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/dist/login-style.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

add_filter( 'login_headerurl', 'codecanal_loginlogo_url' );
function codecanal_loginlogo_url($url)
{
  return home_url();
}

# =========== LATEST NEWS =========== #
	function latest_news() {
		$args = array(
			'post_status'    	=> 'publish',
			'post_type'        	=> 'post',
			'posts_per_page'	=> '6',
		);

		$query = new WP_Query($args);

		foreach($query->posts as $post): ?>
			<?php setup_postdata( $post ); ?>

			<?php echo get_the_title($post->ID) ?>
			<?php echo wp_trim_words($post->post_content, 10)?>
			<?php echo get_the_permalink($post->ID) ?>

		<?php endforeach;

		wp_reset_query();
	}

# =========== ADD ACF OPTIONS =========== #
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

# =========== REMOVE AUTI P CF7 =========== #
    add_filter('wpcf7_autop_or_not', '__return_false');

# =========== REMOVE EMOJIS =========== #
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

# =========== MOVE YOAST TO BOTTOM =========== #
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
