<?php

add_action('tgmpa_register', function () {
    $plugins = [
        [
            'name' => 'Advanced Custom Fields PRO',
            'slug' => 'advanced-custom-fields-pro',
            'source' => 'https://sjp-digital.co.uk/wordpress/acf.zip',
            'required' => true,
        ],
        [
            'name' => 'Migrate DB Pro',
            'slug' => 'migrate-db-pro',
            'source' => 'https://sjp-digital.co.uk/wordpress/migrate-db.zip',
        ],
        [
            'name' => 'Migrate DB Pro Media',
            'slug' => 'migrate-db-pro-media',
            'source' => 'https://sjp-digital.co.uk/wordpress/migrate-db-media.zip',
        ],
        [
            'name' => 'Migrate DB Pro Theme and Plugins',
            'slug' => 'migrate-db-pro-theme-and-plugins',
            'source' => 'https://sjp-digital.co.uk/wordpress/migrate-db-theme.zip',
        ],
        [
            'name' => 'WordPress SEO by Yoast',
            'slug' => 'wordpress-seo',
            'is_callable' => 'wpseo_init',
        ],
        [
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
            'required' => true,
        ],
        [
            'name' => 'Safe SVG',
            'slug' => 'safe-svg',
        ],
        [
            'name' => 'Toolset',
            'slug' => 'types',
            'source' => 'https://sjp-digital.co.uk/wordpress/toolset.zip',
        ],
        [
            'name' => 'Duplicate Post',
            'slug' => 'duplicate-post',
        ],
        [
            'name' => 'Flamingo',
            'slug' => 'flamingo',
        ],
        [
            'name' => 'Classic Editor',
            'slug' => 'classic-editor',
        ],
        [
            'name' => 'WP Database Backup',
            'slug' => 'wp-database-backup',
        ],
        [
            'name' => 'Stream',
            'slug' => 'stream',
        ],
        [
            'name' => 'Smush',
            'slug' => 'wp-smushit',
        ],
    ];

    $config = [
        'id' => 'wordpress-theme-starter',
        'default_path' => '',
        'menu' => 'tgmpa-install-plugins',
        'parent_slug' => 'themes.php',
        'capability' => 'edit_theme_options',
        'has_notices' => true,
        'dismissable' => true,
        'dismiss_msg' => '',
        'is_automatic' => false,
        'message' => '',
    ];

    tgmpa($plugins, $config);
});
