<?php get_header(); ?>

	<?php while (have_posts()) : the_post(); ?>

		<?php the_title(); ?>
		<?php echo get_the_date() ?>

		<?php
		#$thumb_id = get_post_thumbnail_id($post);
		#$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		#$thumb_url = $thumb_url_array[0];
		#echo $thumb_url;
		?>

		<?php the_content(); ?>

	<?php endwhile; ?>

	<?php bones_page_navi(); ?>

<?php get_footer(); ?>
