<?php get_header();

while ( have_rows('blocks', $post->ID) ) : the_row();

	if( get_row_layout() == 'masthead' ):

		include component('masthead.php');

	elseif( get_row_layout() == 'two_col_cta' ):

		include component('two-col-cta.php');

	elseif( get_row_layout() == 'image_content_button' ):

		include component('image-content-button.php');

	elseif( get_row_layout() == 'rows' ):

		include component('rows.php');

	elseif( get_row_layout() == 'icon_strip' ):

		include component('icon-strip.php');

	elseif( get_row_layout() == 'case_study' ):

		include component('case-study.php');

	elseif( get_row_layout() == 'contact_form' ):

		include component('contact-strip.php');

	elseif( get_row_layout() == 'launch_promo' ):

		include component('launch-promo.php');

	endif;

endwhile;

get_footer(); ?>
