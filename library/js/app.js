jQuery(document).ready(function($) {
	var windowWidth = $(window).width()

/* ====================== MOBILE NAV ======================  */
    if(windowWidth < 1024) {
	    $('.menu-toggle').on("click", function() {
	        $(this).toggleClass('active');
	        $('header nav').toggleClass('open');
	        $('header').toggleClass('active');
	        $('body').toggleClass('noscroll');
	    })

	    $('nav li, nav .btn').on("click", function() {
	        $('.menu-toggle').removeClass('active');
	        $('header nav').removeClass('open');
	        $('header').removeClass('active');
	        $('body').removeClass('noscroll');
	    })

	    $('#menu-main-menu .menu-item-has-children').on("click", function() {
	       $(this).toggleClass('expanded');
	       $(this).find('.sub-menu').slideToggle();
	    })
	}

/* ====================== HEADER CLASS ======================  */
  if(windowWidth >= 1024) {
	var height = document.getElementById('header').offsetHeight;
	var ctaHeight = document.getElementById('below-header-cta').offsetHeight;
	var prevScrollpos = window.pageYOffset;
	window.onscroll = function() {
	  var currentScrollPos = window.pageYOffset;
	  if (prevScrollpos > currentScrollPos) {
		document.getElementById("header").style.top = "0";
		document.getElementById("below-header-cta").classList.add('hidden');

	  } else {
		document.getElementById("header").style.top = '-'+height+'px';
		document.getElementById("below-header-cta").classList.remove('hidden');
	  }
	  prevScrollpos = currentScrollPos;
	}
  }

/* ====================== SHOW HEADER CTA ======================  */
	if ($('.promo-strip').length > 0) {
		var promoHeight = $('.promo-strip').height();

		var waypoint = new Waypoint({
		  element: $('.promo-strip'),
		  handler: function(direction) {
		    $(".below-header-cta").toggleClass("active");
		  },
		  offset: -promoHeight
		})
	}

	$('.minimise').on("click", function() {
		$(".below-header-cta").toggleClass("tuck-away");
		$(".below-header-cta-reveal").toggleClass("hidden");
	})

	$('.below-header-cta-reveal').on("click", function() {
		$(".below-header-cta").removeClass("tuck-away");
		$(this).toggleClass("hidden");
	})

	/* ====================== ANIMATE STATS ======================  */
	$('.stat span span').counterUp({
		time: 2000,
		offset: 98,
		beginAt: 0,
	});

/* ====================== PLAY VIDEO ======================  */
    $('video').on('ended',function(){
    	$(this).parent().removeClass('video-playing');
	});

    $('video').click(function() {
        this.paused ? this.play() : this.pause();
    });

	$('video').on('play', function() {
		$(this).parent().addClass('video-playing');
	});
	$('video').on('pause', function() {
		$(this).parent().removeClass('video-playing');
	});

//===== TRIGGER ANIMATION =====//
  $('.animateelement').each(function(){
	$(this).waypoint({
	  handler: function(direction){
		$(this.element).addClass('animate');
	  },
	   offset: '85%'
	 });
  })

/* ====================== CONTACT FORM STRIP ======================  */
	$('.footer-contact-form-strip .btn').click(function() {
		if($(this).hasClass('active-button')){
			$(this).removeClass('active-button');
			$(this).html('Get in touch');
			$('.footer-contact-form').slideUp();
		} else {
			$(this).addClass('active-button');
			$(this).html('Close');
			$('.footer-contact-form').slideDown();
			$('html, body').animate({scrollTop:$('.footer-contact-form').offset().top -95}, 'slow');
		}
	});

/* ====================== CF7 HIDE FORM SHOW THANK YOU ======================  */
	  document.addEventListener( 'wpcf7mailsent', function( event ) {
	  	if(event.detail.contactFormId == '5') {
    		$('.contact-form-container').addClass('mail-sent');
    	} else if(event.detail.contactFormId == '124') {
    		$('.modal-innards-container').addClass('funnel-form-sent');
    	} else if(event.detail.contactFormId == '140') {
    		$('.modal-innards-container').addClass('booking-form-sent');
    	}
	  }, false );

  /* ====================== DISABLE CONTACT FORM BTN UNLESS CHECKED ======================  */
	$('.wpcf7-submit').prop('disabled', true);

	$('.wpcf7-acceptance input').change(function () {
		$(this).parents('form').find('.wpcf7-submit').prop("disabled", !this.checked);
		$(this).parent('label').toggleClass('checked');
	}).change()

	$('.wpcf7-acceptance label').removeClass('checked');

  /* ====================== RESET FORMS ON REMODAL CLOSE ======================  */
	$(document).on('click', '.remodal .remodal-close', function(){
		$('.modal-innards-container').removeClass('booking-form-sent funnel-form-sent');
		$('.modal-innards-container').find('input,textarea,.custom-select').removeClass('valid');

		$('.progress-label span').html('1');
		$('.form-progress .step-two').removeClass('active');
		$('.step-two').removeClass('active-step');
		$('.step-one').removeClass('hidden-step');
		$('.step-one').addClass('active-step');
	})

//================================ BOOK A CALL MODAL ===============================//
	//Fake submit to trigger validation
	.on('click', '.active-step .btn', function(){
		var input = $('.active-step input'),
		form = $('#wpcf7-f140-o3 .wpcf7-form');
		form = $('#wpcf7-f140-o3 .wpcf7-form');
		form.submit();

		  //Hide Error Labels
		$('.step-two label.error').hide();
	    $('.step-two .error').removeClass('error');

	    //If no errors on Step One
        if( !input.hasClass('error') ) {
        	$('.progress-label span').html('2');
        	$('.form-progress .step-two').addClass('active');

        	//Hide Error Labels (Just to be sure)
            $('.step-two label.error').hide();
            $('.step-two .error').removeClass('error');

            //Go to next step
           	$(this).parent('.form-step').removeClass('active-step');
			$(this).parent('.form-step').addClass('hidden-step');
			$(this).parent('.form-step').next('.form-step').addClass('active-step');
			$('.current-marketing-activity textarea').focus();
        }
	})

	//Change button text to subscribe
	$('.book-a-call-modal .thank-you-message .wpcf7-submit').prop('value', 'Subscribe');

	//On intial form submit, populate hidden subscribe form
	$(document).on('click', '#wpcf7-f140-o3 .wpcf7-submit', function(){
		var firstName = $('#wpcf7-f140-o3 .your-first-name input').val()
		var lastName = $('#wpcf7-f140-o3 .your-last-name input').val()
		var email = $('#wpcf7-f140-o3 .your-email input').val()

		$('.book-a-call-modal .thank-you-message .your-first-name input').val(firstName);
		$('.book-a-call-modal .thank-you-message .your-last-name input').val(lastName);
		$('.book-a-call-modal .thank-you-message .your-email input').val(email);
	});

/* ====================== CUSTOM SELECT ======================  */
    $('.custom-select .text').on("click", function() {
        $('.custom-select').toggleClass('active');
        $('.custom-select ul').slideToggle();
    })

    $('.custom-select li').on("click", function() {
    	$('.custom-select ul li').removeClass('active');
		$(this).addClass('active');
		var selectedItem = $(this).text();
		$('.custom-select .text').html(selectedItem);
		$('.custom-select').removeClass('active');
		$('.custom-select').removeClass('error');
		$('.custom-select').addClass('valid');
		$('.custom-select ul').slideUp();

		$('.current-marketing-spend input').val(selectedItem)
	});

    //Validate custom select on hidden field
    $('#wpcf7-f140-o3').on("click", function() {
		 if( $('.current-marketing-spend input').hasClass('error') ) {
			$('.custom-select').addClass('error');
		}
	});

/* ====================== SMOOTH SCROLL ======================  */
// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
	// On-page links
	if (
	  location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
	  &&
	  location.hostname == this.hostname
	) {
	  // Figure out element to scroll to
	  var target = $(this.hash);
	  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	  // Does a scroll target exist?
	  if (target.length) {
		// Only prevent default if animation is actually gonna happen
		event.preventDefault();
		$('html, body').animate({
		  scrollTop: target.offset().top
		}, 1000, function() {
		  // Callback after animation
		  // Must change focus!
		  var $target = $(target);
		  $target.focus();
		  if ($target.is(":focus")) { // Checking if the target was focused
			return false;
		  } else {
			$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
			$target.focus(); // Set focus again
		  };
		});
	  }
	}
  });

})
