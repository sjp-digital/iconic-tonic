<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title(''); ?></title>
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/site.webmanifest">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="theme-color" content="#ffffff">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>

        <? if(get_field('analytics_code', 'options')): ?>
        	<? the_field('analytics_code', 'options') ?>
        <? endif; ?>

       <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
		<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
		<script>
		window.addEventListener("load", function(){
		window.cookieconsent.initialise({
		  "palette": {
		    "popup": {
		      "background": "#1a1a1a",
		      "text": "#ffffff"
		    },
		    "button": {
		      "background": "#FEE81D",
		      "text": "#000"
		    }
		  },
		  "theme": "classic",
		  "position": "bottom-right",
		  "content": {
		    "href": "/cookies-policy/"
		  }
		})});
		</script>

        <script src="https://kit.fontawesome.com/d8ddbdf770.js"></script>
		<link rel="stylesheet" href="https://use.typekit.net/qfx4oxn.css">

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149418967-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-149418967-1');
		</script>

        <!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '640780246433834');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=640780246433834&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
    </head>

	<body <?php body_class(); ?>>

	<nav class="mobile-nav">
		<?php wp_nav_menu(array(
			'menu' => __( 'The Main Menu', 'bonestheme' ),
			'theme_location' => 'main-nav',
		)); ?>
		<a class="btn yellow" href="#book-a-call-modal">Contact</a>
	</nav>

	<header id="header" class="animateelement fadedown">
		<div class="wrap">
			<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow"><img src="<? image('logo.svg') ?>" alt="Logo"></a>

			<nav class="desktop-nav">
				<?php wp_nav_menu(array(
					'menu' => __( 'The Main Menu', 'bonestheme' ),
					'theme_location' => 'main-nav',
				)); ?>
				<a class="btn yellow" href="#book-a-call-modal">Contact</a>
			</nav>

			<div class="menu-toggle">
				<div class="hamburger">
					<span></span>
					<span></span>
				</div>
			</div>
		</div>
	</header>

	<div class="below-header-cta" id="below-header-cta">
		<div class="wrap">
			<h5><? the_field('below_header_cta_text', 'options'); ?></h5>
			<a href="#funnel-modal" class="btn black">Download Funnel Map</a>

		</div>
		<div class="minimise"><img src="<? image('select-down-chevron.svg') ?>" alt="Down Arrow"></div>
	</div>

	<div class="below-header-cta-reveal hidden">
		<img src="<? image('select-down-chevron.svg') ?>" alt="Down Arrow"></div>
	</div>
