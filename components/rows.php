<div class="rows" id="find-out">
	<? while ( have_rows('rows') ) : the_row(); ?>
		<? $image = get_sub_field('row_image', $post->ID); ?>

		<div class="row">
			<div class="wrap">
				<div class="content animateelement fadeup">
					<? the_sub_field('row_content'); ?>
				</div>
				<div class="image-container animateelement fadein">
					<?= wp_get_attachment_image( $image, 'full' ); ?>
				</div>
			</div>
		</div>
	<? endwhile; ?>
</div>
