<div class="footer-contact-form-strip" id="get-in-touch">
	<div class="wrap">
		<img class="icon-one contact-icon" src="<? image('image-one.svg') ?>">
		<img class="icon-two contact-icon" src="<? image('image-two.svg') ?>">

		<div class="content">
			<div class="animateelement fadeup"><? the_sub_field('contact_strip_content', $post->ID) ?></div>
			<div class="btn animateelement fadeup yellow">Get in touch</div>
		</div>
	</div>
</div>

<div class="footer-contact-form contact-form-container wrap">
	<?= do_shortcode('[contact-form-7 id="9" title="Contact form 1"]'); ?>

	<div class="footer-thank-you-message">
		<h3>Thank you for your submission.<br>
		A member of our team will be in touch soon.</h3>
	</div>
</div>
