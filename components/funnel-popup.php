<div class="remodal funnel-modal" data-remodal-id="funnel-modal">
	<div class="modal-innards-container">
		<div class="form-container">
			<a data-remodal-action="close" class="remodal-close"></a>

			<h5><? the_field('funnel_modal_title', 'options'); ?></h5>

			<?= do_shortcode('[contact-form-7 id="124" title="Funnel Popup Form"]') ?>
		</div>

		<div class="funnel-thank-you thank-you-message">
			<a data-remodal-action="close" class="remodal-close"></a>
			<h5><? the_field('thank_you_message', 'options'); ?></h5>
		</div>
	</div>
</div>
