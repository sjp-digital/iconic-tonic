<div class="case-study">
	<div class="wrap">
		<h3 class="animateelement fadeup"><? the_sub_field('main_title', $post->ID); ?></h3>

		<div class="intro">
			<div class="col">
				<? the_sub_field('case_study_left_column_content', $post->ID); ?>
			</div>
			<div class="col solution">
				<? the_sub_field('case_study_right_column_content', $post->ID); ?>
			</div>

			<div class="col stats">
				<h5>Statistics:</h5>
				<div class="stats-container">
					<? if(get_sub_field('stat_one', $post->ID)): ?>
						<p class="stat"><? the_sub_field('stat_one', $post->ID); ?></p>
					<? endif; ?>
					<? if(get_sub_field('stat_two', $post->ID)): ?>
						<p class="stat"><? the_sub_field('stat_two', $post->ID); ?></p>
					<? endif; ?>
				</div>
			</div>
		</div>

		<div class="lower-row">
			<div class="media animateelement fadein">
				<? if(get_sub_field('video_or_image', $post->ID) == 'video'): ?>
					<div class="video-container">
						<video src="<? the_sub_field('video', $post->ID); ?>" poster="<? the_sub_field('video_poster', $post->ID) ?>"></video>
					</div>
				<? else: ?>
					<? $image = get_sub_field('image', $post->ID); ?>
					<?= wp_get_attachment_image( $image, 'full' ); ?>
				<? endif; ?>
			</div>
		</div>
	</div>
</div>
