<? if(get_sub_field('left_column_background_colour', $post->ID) == 'black'):
	$leftbuttonclass = 'turquoise';
else:
	$leftbuttonclass = 'black';
endif; ?>

<? if(get_sub_field('right_column_background_colour', $post->ID) == 'black'):
	$rightbuttonclass = 'turquoise';
else:
	$rightbuttonclass = 'black';
endif; ?>

<div class="two-col-cta<? if(get_sub_field('promo_strip', $post->ID)): ?> promo-strip<? endif; ?>">
	<div class="wrap">
		<div class="left-col col <? the_sub_field('left_column_background_colour', $post->ID) ?>">
			<div class="animateelement faderight"><? the_sub_field('left_col_content', $post->ID); ?></div>

			<? $btn = get_sub_field('left_col_button', $post->ID); ?>
			<? if($btn): ?>
				<a class="btn animateelement fadein <? the_sub_field('left_button_alignment', $post->ID); ?> <?= $leftbuttonclass; ?>" href="<?php echo $btn['url']; ?>"><?php echo $btn['title']; ?></a>
			<? endif; ?>
		</div>

		<div class="right-col col <? the_sub_field('right_column_background_colour', $post->ID) ?>">
			<div class="animateelement fadeleft"><? the_sub_field('right_col_content', $post->ID); ?></div>

			<? $btn =  get_sub_field('right_col_button', $post->ID); ?>
			<? if($btn): ?>
				<a class="btn animateelement fadein <? the_sub_field('right_button_alignment', $post->ID); ?> <?= $rightbuttonclass; ?>" href="<?php echo $btn['url']; ?>"><?php echo $btn['title']; ?></a>
			<? endif; ?>
		</div>
	</div>
</div>
