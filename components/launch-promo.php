<? $spots =  get_sub_field('spots_remaining_text', $post->ID); ?>

<div class="launch-promo">
	<div class="wrap">
		<div class="content animateelement fadein">
			<? the_sub_field('launch_promo_content', $post->ID); ?>
		</div>
		<div class="counter animateelement fadein">

			<p>Counter</p>

			<h3 class="h1">Only <strong><?= $spots; ?></strong> spot<? if($spots !== '1'): ?>s<?endif;?> left</h3>
			<? $image = get_sub_field('spots_remaining', $post->ID); ?>
			<?= wp_get_attachment_image( $image, 'full' ); ?>

			<a href="#get-in-touch" class="btn yellow">Book a call now</a>
		</div>
	</div>
</div>


