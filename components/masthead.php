<div class="masthead">
	<div class="wrap">
		<div class="left-col">
			<div class="content animateelement faderight">
				<? the_sub_field('masthead_content', $post->ID); ?>
			</div>

			<? $btn =  get_sub_field('masthead_button', $post->ID); ?>
			<? if($btn): ?>
				<a class="btn yellow animateelement faderight" href="<?php echo $btn['url']; ?>"><?php echo $btn['title']; ?></a>
			<? endif; ?>
		</div>

		<div class="media animateelement fadein">
			<? $image = get_sub_field('svg', $post->ID); ?>

			<? if(get_sub_field('media_format') == 'svg'): ?>
				<?= wp_get_attachment_image( $image, 'full' ); ?>
			<? else: ?>
				<? the_sub_field('loom_embed', $post->ID); ?>
			<? endif; ?>
		</div>

	</div>
</div>
