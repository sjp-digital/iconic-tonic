<? if(get_sub_field('background_colour', $post->ID) == 'black'):
	$backgroundcolour = 'black';
else:
	$backgroundcolour = get_sub_field('background_colour', $post->ID);
endif; ?>

<div class="icon-strip <?= $backgroundcolour; ?>">
	<div class="wrap">
		<h3 class="h3 animateelement fadeup"><? the_sub_field('strip_title', $post->ID) ?></h3>

		<div class="icons">
			<? while ( have_rows('icons') ) : the_row(); ?>
				<div class="icon animateelement fadeup">
					<img src="<? the_sub_field('icon'); ?>" alt="Icon">
					<? the_sub_field('icon_content', $post->ID); ?>
				</div>
			<? endwhile; ?>
		</div>
	</div>

	<a href="#get-in-touch" class="btn yellow">Get in touch</a>
</div>
