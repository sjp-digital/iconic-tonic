<div class="image-content-button">
	<div class="wrap">
		<? $image = get_sub_field('image', $post->ID); ?>
		<?= wp_get_attachment_image( $image, 'full' ); ?>

		<div class="right-col">
			<div class="animateelement fadeleft"><? the_sub_field('content', $post->ID); ?></div>
			<? $btn =  get_sub_field('button', $post->ID); ?>
			<? if($btn): ?>
				<a class="btn yellow animateelement fadein <? the_sub_field('right_button_alignment', $post->ID); ?> <?= $rightbuttonclass; ?>" href="<?php echo $btn['url']; ?>"><?php echo $btn['title']; ?></a>
			<? endif; ?>
		</div>
	</div>
</div>
