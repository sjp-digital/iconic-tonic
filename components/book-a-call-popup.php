<div class="remodal book-a-call-modal" data-remodal-id="book-a-call-modal">
	<div class="modal-innards-container">
		<div class="form-container">
			<a data-remodal-action="close" class="remodal-close"></a>

			<p class="progress-label">Step <span>1</span> of 2</p>
			<div class="form-progress">
				<div class="step step-one"></div>
				<div class="step step-two"></div>
			</div>
			<?= do_shortcode('[contact-form-7 id="140" title="Book a Call Popup Form"]') ?>
		</div>

		<div class="thank-you-message book-thank-you">
			<a data-remodal-action="close" class="remodal-close"></a>
			<h5><? the_field('book_thank_you_message', 'options'); ?></h5>
			<p><? the_field('subscribe_prompt', 'options'); ?></p>

			<?= do_shortcode('[contact-form-7 id="124" title="Funnel Popup Form"]'); ?>
		</div>

		<div class="funnel-thank-you thank-you-message">
			<a data-remodal-action="close" class="remodal-close"></a>
			<h5><? the_field('thank_you_message', 'options'); ?></h5>
		</div>
	</div>
</div>
