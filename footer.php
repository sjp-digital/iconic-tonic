
			<footer>
				<div class="wrap">
					<div class="left-col">
						<img src="<? image('white-logo.svg') ?>" alt="Footer Logo">
						<p><? the_field('address', 'option'); ?></p>
						<p><? the_field('company_number', 'option'); ?></p>
					</div>
					<div class="social-container">
						<a href="<? the_field('facebook', 'option') ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
						<a href="<? the_field('twitter', 'option') ?>" target="_blank"><i class="fab fa-twitter"></i></a>
						<a href="<? the_field('linkedin', 'option') ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
						<a href="<? the_field('instagram', 'option') ?>" target="_blank"><i class="fab fa-instagram"></i></a>
					</div>
				</div>
			</footer>

		<? include component('funnel-popup.php'); ?>
		<? include component('book-a-call-popup.php'); ?>

		<?php wp_footer(); ?>

		<script type="text/javascript"> _linkedin_partner_id = "1915793"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1915793&fmt=gif" /> </noscript>
	</body>

</html>
